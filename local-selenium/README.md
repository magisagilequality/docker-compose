## Dolphin automate

1. Install docker and docker-compose
2. Modify {YOUR_OS}.yml file with:
    * CLIENT_KEY={YOUR_DOLPHIN_CLIENT_KEY}
    * CLIENT_SECRET={YOUR_DOLPHIN_CLIENT_SECRET}
    * SELENIUM_HOST={YOUR_LOCAL_IP}
3. Execute into command line: *docker-compose -f {YOUR_OS}.yml up -d*
4. Run dolphin executions

**Note**
For distroy containers type: *docker-compose -f {YOUR_OS}.yml down*